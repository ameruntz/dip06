module top( input clk, reset,
  output [31:0] writedata, dataadr,
  output memwrite);

wire [31:0] pc, instr, readdata;
wire clk_70MHZ;

clk_70 cl(clk,clk_70MHZ,reset);

mips cpu(clk_70MHZ, reset, pc, instr, memwrite, dataadr,
  writedata, readdata);

imem imem(pc[7:2],instr);
dmem dmem(clk_70MHZ,memwrite, dataadr, writedata,
  readdata);

endmodule

