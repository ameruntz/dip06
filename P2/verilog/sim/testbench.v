module testbench;
reg clk;
reg reset;
wire [31:0] writedata, dataadr;
wire memwrite;

top dut(clk,reset, writedata, dataadr, memwrite);

initial begin
  $dumpfile("top.vcd");
  $dumpvars(0,testbench);
end

initial begin
  reset = 1; #42; reset = 0;
  clk = 0;
end

always 
  #5 clk <= ~clk; 

always @(negedge clk) begin
  if (memwrite) 
    if (dataadr === 84 & writedata === 7) begin
      $display("Simulation success!");
      $finish;
    end
end
endmodule
